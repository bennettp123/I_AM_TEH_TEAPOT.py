#!/usr/bin/env python

import SimpleHTTPServer, SocketServer
import urlparse, os

PORT = 3000

print('I AM TEH TEAPOT.py')

class TeapotHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):

        self.send_response(200)
        self.send_header('Content-Type', 'text/html')
        self.end_headers()
        self.wfile.write('''
<html>
<head>
<title>i-am-the-teapot.py</title>
</head>
<body>
 <h1>I AM TEH TEAPOT.py</h1>
</body>
</html>
''')

Handler = TeapotHandler

httpd = SocketServer.TCPServer(("", PORT), Handler)

print "serving at port", PORT
httpd.serve_forever()
